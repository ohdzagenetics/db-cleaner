#!/bin/bash

###
### This script works alongside the db-archiver and removed unnecessary backups
###

# where backups are saved to
backuppath=$BACKUP_PATH

# interval between backups in seconds
interval=$BACKUP_INTERVAL

# Function: sql_files_to_delete takes a directory and finds all DB backups more than 30 days old, sorts them by
# last modified time, then returns all but the most recent backup in that list
# 
# Intent: this function is intended to run across the $backuppath and delete all but the last backup of each month
# and keep the last month's worth of backups
sql_files_to_delete () {
	find $1 -type f -printf -mtime +30 '%T@ %p\n' -iname "*.sql.gz" | \
		sort -n | \
		head -n -1 | \
		cut -f2- -d" "
}

while [ 1 ]
do
	# wait for some time to run again
	sleep $interval
	
	# Find directories without any subdirectories (i.e., the leaves of $backuppath tree)
	# -type d finds only directories, -links 2 means only those with two hardlinks (self and parent-to-child)
	# These deepest directories are then cleaned according to files +30 days old exclude newest file
	for dir in $(find $backuppath -type d -links 2); do
		rm $(sql_files_to_delete $dir)
	done

	# Cleanup empty directories in $backuppath
	find $backuppath -type d -empty -exec rmdir {} \;
done
