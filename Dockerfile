FROM debian:jessie

ENV DEBIAN_FRONTEND noninteractive

ADD cleanup.sh /cleanup.sh
RUN chmod 0755 /cleanup.sh

ENTRYPOINT ["/cleanup.sh"]
